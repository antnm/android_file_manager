import 'dart:io';
import 'package:flutter/material.dart';
import 'utils.dart';
import 'item.dart';
import 'main_app_bar.dart';
import 'create_dialog.dart';
import 'rename_dialog.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => MainPageState();
}

enum ClipboardMode { copy, cut }

class MainPageState extends State<MainPage> {
  static const root = "/storage/emulated/0";
  var _currentDirectory = Directory(root);
  var items = <ItemController>[];
  final clipboard = <FileSystemEntity>[];
  var clipboardMode = ClipboardMode.copy;

  void updateItems([List<FileSystemEntity>? list]) => setState(() {
        list ??= currentDirectory.listSync();
        items.clear();
        for (var entity in list!) {
          items.add(ItemController(entity: entity, selected: false));
          items
              .sort((a, b) => baseName(a.entity).compareTo(baseName(b.entity)));
        }
      });

  set currentDirectory(Directory directory) {
    try {
      updateItems(directory.listSync());
    } on FileSystemException {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Cannot access directory ${baseName(directory)}")));
      return;
    }
    _currentDirectory = directory;
  }

  Directory get currentDirectory => _currentDirectory;

  void addToClipboard() => setState(() {
        clipboard.clear();
        for (var item in items) {
          if (item.selected) {
            clipboard.add(item.entity);
            item.selected = false;
          }
        }
      });

  MainPageState() {
    requestStorage().then((v) => updateItems());
  }

  void delete() {
    var resultingItems = <ItemController>[];
    for (var item in items) {
      if (item.selected) {
        item.entity.delete(recursive: true);
        clipboard.remove(item.entity);
      } else {
        resultingItems.add(item);
      }
    }
    setState(() => items = resultingItems);
  }

  void rename() =>
      showDialog<String>(context: context, builder: (context) => RenameDialog())
          .then((newName) {
        if (newName != null) {
          final first = items.firstWhere((item) => item.selected);
          first.entity.rename(first.entity.parent.path + "/" + newName).then(
              (entity) => setState(() {
                    clipboard.remove(first.entity);
                    updateItems();
                  }),
              onError: (err) => ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text("Cannot rename to $newName"))));
        }
      });

  void create() => showDialog<CreateDialogResult>(
          context: context,
          builder: (context) => CreateDialog()).then((result) {
        if (result != null) {
          switch (result.type) {
            case CreateType.file:
              File(currentDirectory.path + "/" + result.name).create().then(
                  (directory) => updateItems(),
                  onError: (err) => ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          content: Text("Cannot create file " + result.name))));
              break;
            case CreateType.folder:
              Directory(currentDirectory.path + "/" + result.name)
                  .create()
                  .then((directory) => updateItems(),
                      onError: (err) => ScaffoldMessenger.of(context)
                          .showSnackBar(
                              SnackBar(
                                  content: Text(
                                      "Cannot create folder " + result.name))));
              break;
          }
        }
      });

  void paste() async {
    switch (clipboardMode) {
      case ClipboardMode.copy:
        for (var entity in clipboard) {
          if (entity is File) {
            await copyFile(
                entity, currentDirectory.path + "/" + baseName(entity));
          } else if (entity is Directory) {
            if (currentDirectory.path.startsWith(entity.path)) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                      "Cannot copy folder ${baseName(entity)} onto itself")));
              continue;
            }
            await copyDirectory(
                entity, currentDirectory.path + "/" + baseName(entity));
          }
        }
        break;
      case ClipboardMode.cut:
        for (var entity in clipboard) {
          if (currentDirectory.path.startsWith(entity.path)) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content:
                    Text("Cannot cut folder ${baseName(entity)} onto itself")));
            continue;
          }
          await renameEntity(
              entity, currentDirectory.path + "/" + baseName(entity));
        }
        break;
    }
    setState(() => clipboard.clear());
    updateItems();
  }

  @override
  Widget build(BuildContext context) {
    var mode = MainAppBarMode.noneSelectedEmptyClipboard;
    final selected = items.where((i) => i.selected).length;
    if (selected == 0 && clipboard.isNotEmpty) {
      mode = MainAppBarMode.noneSelectedWithClipboard;
    } else if (selected == 1) {
      mode = MainAppBarMode.oneSelected;
    } else if (selected > 1) {
      mode = MainAppBarMode.multipleSelected;
    }
    return Scaffold(
        appBar: MainAppBar(mode,
            onGoBack: () => currentDirectory = currentDirectory.parent,
            onClear: () => setState(() {
                  for (var item in items) {
                    item.selected = false;
                  }
                }),
            onCopy: () {
              clipboardMode = ClipboardMode.copy;
              addToClipboard();
            },
            onDelete: delete,
            onCut: () {
              clipboardMode = ClipboardMode.cut;
              addToClipboard();
            },
            onRename: rename,
            onCreate: create,
            onPaste: paste),
        body: items.isEmpty
            ? const Center(
                child: Text("Empty folder",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                        fontSize: 20.0)))
            : ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) => Item(
                    controller: items[index],
                    onSelected: () => setState(
                        () => items[index].selected = !items[index].selected),
                    onEnter: () {
                      final entity = items[index].entity;
                      if (entity is File) {
                        entity
                            .readAsString()
                            .then((file) => Navigator.pushNamed(
                                context, '/file-edit',
                                arguments: file))
                            .then((text) {
                          if (text != null) {
                            entity.writeAsString(text as String);
                          }
                        });
                      } else if (entity is Directory) {
                        currentDirectory = entity;
                      }
                    })));
  }
}
