import 'package:flutter/material.dart';

enum CreateType { file, folder }

class CreateDialogResult {
  final CreateType type;
  final String name;

  CreateDialogResult(this.type, this.name);
}

class CreateDialog extends Dialog {
  final controller = TextEditingController();

  CreateDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Dialog(
      child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(controller: controller),
            Row(children: [
              TextButton(
                  child: const Text("Create file"),
                  onPressed: () => Navigator.pop(context,
                      CreateDialogResult(CreateType.file, controller.text))),
              const Spacer(),
              TextButton(
                  child: const Text("Create folder"),
                  onPressed: () => Navigator.pop(context,
                      CreateDialogResult(CreateType.folder, controller.text)))
            ])
          ])));
}
