import 'dart:io';
import 'package:permission_handler/permission_handler.dart';

Future<void> requestStorage() =>
    Permission.manageExternalStorage.request().then((status) {
      if (!status.isGranted) {
        requestStorage();
      } else {
        return;
      }
    });

String baseName(FileSystemEntity entity) => entity.path.split("/").last;

final duplicateRegExp = RegExp(r"(.*)\s\((\d)\)$");

Future<String> getAvailablePath(String path) async {
  final type = await FileSystemEntity.type(path);
  if (type == FileSystemEntityType.notFound) {
    return path;
  } else {
    var duplicateCount = 0;
    final match = duplicateRegExp.firstMatch(path);
    if (match == null) {
      duplicateCount = 1;
    } else {
      duplicateCount = int.parse(match.group(2)!);
      path = match.group(1)!;
    }
    while (await FileSystemEntity.type(path + " ($duplicateCount)") !=
        FileSystemEntityType.notFound) {
      duplicateCount++;
    }
    return path + " ($duplicateCount)";
  }
}

Future<File> copyFile(File file, String newPath) async {
  final path = await getAvailablePath(newPath);
  return file.copy(path);
}

Future<Directory> copyDirectory(Directory directory, String newPath) async {
  final path = await getAvailablePath(newPath);
  final newDirectory = await Directory(path).create();
  await for (var entity in directory.list()) {
    if (entity is File) {
      await entity.copy(path + "/" + baseName(entity));
    } else if (entity is Directory) {
      await copyDirectory(entity, path + "/" + baseName(entity));
    }
  }
  return newDirectory;
}

Future<FileSystemEntity> renameEntity(
    FileSystemEntity entity, String newPath) async {
  if (entity.path != newPath) {
    final path = await getAvailablePath(newPath);
    return entity.rename(path);
  }
  return entity.rename(newPath);
}
