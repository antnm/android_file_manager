import 'package:flutter/material.dart';
import 'main_page.dart';
import 'file_edit_page.dart';

void main() => runApp(MaterialApp(
    title: 'File Manager',
    theme: ThemeData(
      primarySwatch: Colors.red,
    ),
    home: const MainPage(),
    routes: {'/file-edit': (context) => const FileEditPage()}));
