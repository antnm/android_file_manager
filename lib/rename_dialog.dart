import 'package:flutter/material.dart';

class RenameDialog extends Dialog {
  final controller = TextEditingController();

  RenameDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Dialog(
      child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(controller: controller),
            TextButton(
                child: const Text("Rename"),
                onPressed: () => Navigator.pop(context, controller.text))
          ])));
}
