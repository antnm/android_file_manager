import 'package:flutter/material.dart';

class FileEditPage extends StatefulWidget {
  const FileEditPage({Key? key}) : super(key: key);

  @override
  State<FileEditPage> createState() => FileEditPageState();
}

class FileEditPageState extends State<FileEditPage> {
  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(
        text: ModalRoute.of(context)!.settings.arguments as String);
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
                icon: const Icon(Icons.cancel),
                tooltip: "Cancel",
                onPressed: () => Navigator.pop(context)),
            actions: [
              IconButton(
                  icon: const Icon(Icons.save),
                  tooltip: "Save",
                  onPressed: () => Navigator.pop(context, controller.text))
            ]),
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controller,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              expands: true,
              decoration: const InputDecoration(
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none),
            )));
  }
}
