import 'package:flutter/material.dart';

enum MainAppBarMode {
  noneSelectedEmptyClipboard,
  noneSelectedWithClipboard,
  oneSelected,
  multipleSelected
}

class MainAppBar extends AppBar {
  final MainAppBarMode mode;
  final VoidCallback onGoBack,
      onClear,
      onCopy,
      onDelete,
      onCut,
      onRename,
      onCreate,
      onPaste;

  MainAppBar(this.mode,
      {required this.onGoBack,
      required this.onClear,
      required this.onCopy,
      required this.onDelete,
      required this.onCut,
      required this.onRename,
      required this.onCreate,
      required this.onPaste,
      Key? key})
      : super(
            key: key,
            leading: IconButton(
                icon: const Icon(Icons.arrow_back), onPressed: onGoBack),
            actions: []) {
    if (mode == MainAppBarMode.noneSelectedEmptyClipboard ||
        mode == MainAppBarMode.noneSelectedWithClipboard) {
      actions!.add(IconButton(
          icon: const Icon(Icons.add),
          tooltip: "Create new file or folder",
          onPressed: onCreate));
      if (mode == MainAppBarMode.noneSelectedWithClipboard) {
        actions!.insert(
            0,
            IconButton(
                icon: const Icon(Icons.paste),
                tooltip: "Paste",
                onPressed: onPaste));
      }
    } else {
      actions!.addAll([
        IconButton(
            icon: const Icon(Icons.clear),
            tooltip: "Clear",
            onPressed: onClear),
        IconButton(
            icon: const Icon(Icons.copy), tooltip: "Copy", onPressed: onCopy),
        IconButton(
            icon: const Icon(Icons.delete),
            tooltip: "Delete",
            onPressed: onDelete),
        IconButton(
            icon: const Icon(Icons.cut), tooltip: "Cut", onPressed: onCut)
      ]);
      if (mode == MainAppBarMode.oneSelected) {
        actions!.insert(
            1,
            IconButton(
                icon: const Icon(Icons.edit),
                tooltip: "Rename",
                onPressed: onRename));
      }
    }
  }
}
