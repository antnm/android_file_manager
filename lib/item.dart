import 'dart:io';
import 'package:flutter/material.dart';
import 'utils.dart';

class ItemController {
  FileSystemEntity entity;
  bool selected;

  ItemController({required this.entity, required this.selected});
}

class Item extends StatelessWidget {
  final ItemController controller;
  final VoidCallback onSelected;
  final VoidCallback onEnter;

  const Item(
      {required this.controller,
      required this.onSelected,
      required this.onEnter,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) =>
      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        IconButton(
            icon: Icon(controller.selected
                ? Icons.check_circle
                : controller.entity is File
                    ? Icons.file_present
                    : Icons.folder),
            color: controller.selected
                ? Theme.of(context).colorScheme.primary
                : null,
            onPressed: () => onSelected()),
        Expanded(
            child: TextButton(
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(baseName(controller.entity))),
          onPressed: onEnter,
        ))
      ]);
}
